#!/bin/bash

dir=/usr/share/nginx/html

mode="${ARCHIVE:-archive}"

type="silverblue"


if [ "$1" = "" ]; then
  echo you must pass a fedora relese number as parameter
  exit 1
fi

release="$1"

if [ "$2" != "" ]; then
  type="$1"
  release="$2"
fi

if [ ! -f "$dir/config" ]; then

  ostree "--repo=$dir" init "--mode=$mode"

  ostree "--repo=$dir" remote add \
                --contenturl=mirrorlist=https://ostree.fedoraproject.org/mirrorlist \
                --no-gpg-verify \
                fedora 'https://ostree.fedoraproject.org'
fi

ostree "--repo=$dir" pull --mirror "fedora:fedora/$release/x86_64/$type"
