FROM docker.io/nginx

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl \
    wget \
    ostree

COPY update.sh /usr/local/bin/update.sh
COPY nginx-default.conf /etc/nginx/conf.d/default.conf
